<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Blog</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>

<body>
    <section class="form">
        <div class="row">
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-md-10 ">
                <div class="card  ">
                    <div class="card-header">
                        Edit User
                    </div>
                    <div class="card-body">
                        <form action="process.php" method="post">
                            <div class="row">
                                <div class="col-md-6 ">
                                    <label class="form-label">Title</label>
                                    <input type="text" class="form-control" id="title" name="title" value="">
                                    <input type="hidden" class="form-control" id="edit_id" name="edit_id">
                                </div>
                                <div class="col-md-6">
                                    <label class="form-label">Upload File</label>
                                    <input type="file" class="form-control" id="fileUpload" name="fileUpload" value="">
                                </div>
                                <div class="col-12 mt-3">
                                    <label class="form-label">Description</label>
                                    <textarea class="form-control" rows="7" id="description" name="description" value=""></textarea>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label for="inputState" class="form-label">Author's Name</label>
                                    <input type="text" class="form-control" id="authorName" name="authorName" value="">
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-2 mt-5">
                                    <button type="reset" class="btn btn-light ">Reset</button>
                                    <button type="submit" class="btn btn-success" id="edit" name="edit" value="">Update</button>
                                </div>
                                <div class="col-md-4  mt-3">
                                    <img src="assets/images/image.jpeg" class="image1 img-fluid">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <a href="index.php" class="btn btn-info " id="back" name="back" value="">Back</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-1"></div>
        </div>
    </section>
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>