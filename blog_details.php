<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog Details</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>

<body>
    <div class="container mt-2 ">
        <h2 class="text-center mb-5">This is Sample Title</h2>
        <img src="assets/images/image.jpeg" class="image img-fluid" width="600px" height="400px" >
        <p class="text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sequi recusandae nisi repellat nostrum, quos dolore aspernatur, beatae molestiae, voluptatum cupiditate earum sed sapiente laudantium quas. In sequi ipsum ex doloremque quis dolorum dolor praesentium eaque, dolorem quidem. Laboriosam, velit consequuntur. Amet optio sint voluptatibus quia illum, eos et? Quas, illum natus quibusdam totam dolores doloribus consequatur. Omnis tempora nemo ipsa ullam pariatur alias excepturi. Distinctio ex illo blanditiis harum perspiciatis illum saepe, quos asperiores quo quam quis mollitia. Possimus, molestias sunt soluta iure eius quo eum repellendus non cupiditate optio? Corporis animi ab aliquid fugit? Voluptate cupiditate itaque atque magnam perspiciatis, ipsa debitis a voluptatibus perferendis inventore facere repellendus nemo illum nobis. Repellendus vero, sequi aperiam eaque nam aut repellat ullam, eligendi accusantium aspernatur iusto recusandae molestiae? Similique explicabo sed veritatis facilis error ducimus. Perferendis pariatur similique reiciendis veritatis aliquid, totam doloremque maxime aliquam sapiente maiores, ducimus mollitia autem ea, necessitatibus provident nobis repudiandae dolor possimus est aperiam explicabo architecto.
            <br>
            <br>
            Exercitationem harum explicabo numquam nesciunt id, maxime itaque! Rem nostrum sunt explicabo non corrupti quisquam esse minima quae doloremque modi doloribus earum accusamus, sint, harum in suscipit ipsa et facere? Sapiente voluptates rem aliquid optio iusto expedita repellat! Perspiciatis at ipsum neque perferendis qui asperiores odit repudiandae? Repellat recusandae quidem eum magni consequuntur ipsam perspiciatis distinctio modi? Illum voluptates, sapiente expedita, a dicta est distinctio officiis iusto mollitia nulla reprehenderit similique? Magni ipsam non deserunt aliquid. Numquam dolor consequatur pariatur quia ipsam harum aspernatur perspiciatis aliquid sunt itaque, unde iste impedit enim aperiam voluptates quae eveniet voluptatibus laborum molestiae fugit natus reiciendis cum fuga eum. Corrupti aperiam neque at qui aliquid perspiciatis nam rem soluta asperiores incidunt! Soluta, qui? Tempore vel aperiam maxime neque! Fugiat facilis, nam reiciendis, pariatur perferendis, excepturi doloribus tempore earum velit hic molestiae! At maiores quos veritatis dignissimos aspernatur! Accusamus inventore eos mollitia exercitationem corporis debitis possimus tempore officiis impedit omnis, harum obcaecati, repellat enim autem veniam nulla id sint. Ea quos aut voluptates nobis dolores facere et error, corporis incidunt cumque qui. Aliquid reiciendis vitae natus voluptas earum non quibusdam cumque ipsam minus facilis molestiae aperiam quidem fugit, et aspernatur iusto praesentium libero, sit minima officia, possimus incidunt iste. Nam est, facere cumque error delectus provident dignissimos maiores, ad cum esse alias earum eum beatae repudiandae amet animi necessitatibus corporis commodi! Amet architecto accusantium commodi eius velit nesciunt debitis iusto praesentium, culpa animi voluptate dolor, est ducimus in non a deserunt quos labore, fugit veniam eum corrupti odit!
            <br>
            <br> Veniam eos dolorem, deleniti excepturi assumenda dignissimos animi aut ipsum repellendus a, similique velit quibusdam odit culpa voluptatibus consectetur delectus ab accusantium! Aut facere accusantium at. Facilis sit mollitia facere dolorem deleniti molestias pariatur porro nobis ducimus enim quisquam alias inventore cupiditate sapiente quia, cumque accusantium error. Inventore, repellat. Ratione, consequuntur ad. Quaerat harum adipisci assumenda quo sunt dicta corrupti, ratione perferendis accusamus excepturi quia eaque tenetur a totam sint nam odit facere eius perspiciatis at dignissimos pariatur autem! Ratione, ipsa. Perspiciatis ut non sunt provident natus.
        </p>
        <h5 class="text-end"    >Author's Name</h5>
    </div>
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>