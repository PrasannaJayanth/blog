<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/fontawesome.min.js" integrity="sha512-64O4TSvYybbO2u06YzKDmZfLj/Tcr9+oorWhxzE3yDnmBRf7wvDgQweCzUf5pm2xYTgHMMyk5tW8kWU92JENng==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body>

    <section class="form1 mt-5">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="add.php" class="btn btn-danger" style="float: right; width: 100px;">Add Blog</a>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-lg-4 col-md-6 col-12 ">
                    <div class="card mb-4">
                        <a href="edit.php" class="edit"><i class="fa-solid fa-pen-to-square" style=" color: #7cbe45;"></i></a>
                        <a href="#" class="edit1"><i class="fa-solid fa-trash" style="color: red;"></i></i></a>

                        <a href="blog_details.php">
                            <img src="assets/images/image.jpeg" class="card-img-top">
                        </a>
                        <div class=" card-body">
                            <h5 class="card-title">This is Sample Title</h5>
                            <p class="card-text ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae odit repudiandae quis, voluptatem facere aut sint ipsam asperiores assumenda repellat voluptates modi eos quaerat, incidunt fugiat vel nihil, cupiditate illo rem qui laborum possimus quidem! Mollitia natus a magni voluptate libero deleniti facere dolorum ex officiis eum debitis, quam nobis. Qui exercitationem, odio ipsam culpa laborum magnam incidunt eius perspiciatis natus! Repellendus veniam eos itaque omnis nobis consequatur sint perspiciatis soluta, cum eligendi maxime aut corrupti. Aspernatur dignissimos nesciunt iste optio in nulla, odit et fugiat magni qui quo veritatis consequuntur repellat deserunt labore libero saepe sit tenetur ipsam aperiam facilis ducimus quasi perferendis. Eligendi sunt sit dolor, repudiandae fugit officiis eaque reiciendis, vel nobis asperiores, sequi autem pariatur voluptate placeat nihil minima dolorum accusamus. Nisi quaerat libero possimus asperiores expedita dolorem voluptatem minima doloribus delectus aut iusto distinctio voluptatum ipsa in enim accusamus, similique sequi totam nemo atque deleniti illo maxime. Voluptas officia eveniet molestias, at pariatur odio. Sapiente rem praesentium quis harum mollitia assumenda eum rerum nemo illum fugit blanditiis aliquid, eveniet veritatis optio aut aspernatur atque nostrum omnis inventore. Magnam excepturi omnis debitis aliquam quasi inventore cupiditate consequuntur, magni repellendus, nostrum illo nemo illum amet in sit commodi doloribus facere temporibus, saepe at soluta quos nobis? Sapiente, nemo pariatur! Dicta animi, minima autem a perspiciatis maiores. Numquam voluptatem officia alias commodi pariatur unde odio eligendi et repudiandae tenetur aspernatur incidunt amet sequi ipsa, neque laborum? Numquam quae a hic nihil delectus inventore laboriosam ipsam molestiae, saepe sint veritatis sed et! Repudiandae mollitia eligendi quis reiciendis, id aperiam ipsum harum facilis error placeat. Modi harum necessitatibus odio veniam. Delectus odit eaque, laboriosam praesentium in fuga obcaecati consequatur adipisci excepturi numquam magnam! Possimus voluptate quisquam distinctio nulla qui, eaque quis obcaecati laboriosam vero, in fuga! Tempora molestias, aliquam nam ducimus, placeat mollitia quod ut quo reiciendis amet, architecto consectetur sequi sed adipisci doloribus enim nesciunt ratione consequuntur? Veritatis aliquid animi suscipit nostrum itaque doloremque ex eos libero quo veniam deleniti excepturi at, eligendi expedita corrupti recusandae labore accusantium? Sit, perferendis illum? Placeat repellat molestias aliquam est vitae dignissimos debitis modi similique maxime excepturi fuga consequuntur ratione, at ipsum eum id animi blanditiis eligendi dolor officia sit numquam iusto quibusdam mollitia? Optio necessitatibus exercitationem delectus veritatis, aspernatur velit. Nemo, ducimus? At eveniet magni ipsa labore quasi voluptatum architecto iusto eligendi nisi tempore, necessitatibus sed quidem alias eum, quaerat fugiat molestiae, quos libero quia. Quos, optio libero reprehenderit pariatur ipsam fugit aut harum debitis dolor animi, voluptate accusantium recusandae itaque voluptas praesentium illo. Non fuga reiciendis nobis doloribus soluta quos sed, temporibus doloremque illo magni, neque deleniti, est aut quidem labore asperiores in amet commodi ratione! Aspernatur exercitationem modi maiores similique rem! Dolore vero, nam excepturi saepe similique, sequi vel tenetur, magni quas beatae dicta molestiae dolor mollitia debitis accusamus accusantium ratione dignissimos nulla nostrum praesentium. Fuga nam quis ab libero nesciunt sunt odio, eveniet illum mollitia voluptas, vitae et, dicta reiciendis expedita eaque in nemo soluta possimus. Earum, asperiores nihil..</p>
                            <footer class="blockquote-footer "> Author Name</footer>
                            <a href="blog_details.php" class="btn btn-primary mb-1 text-decoration-none text-light">Read More
                                <i class="fa-solid fa-arrow-right ms-2" style="color: white"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-lg-4 col-md-6 col-12 ">
                    <div class="card mb-4">
                        <a href="edit.php" class="edit"><i class="fa-solid fa-pen-to-square" style=" color: #7cbe45;"></i></a>
                        <a href="#" class="edit1"><i class="fa-solid fa-trash" style="color: red;"></i></i></a>

                        <a href="blog_details.php">
                            <img src="assets/images/image.jpeg" class="card-img-top">
                        </a>
                        <div class=" card-body">
                            <h5 class="card-title">This is Sample Title</h5>
                            <p class="card-text ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae odit repudiandae quis, voluptatem facere aut sint ipsam asperiores assumenda repellat voluptates modi eos quaerat, incidunt fugiat vel nihil, cupiditate illo rem qui laborum possimus quidem! Mollitia natus a magni voluptate libero deleniti facere dolorum ex officiis eum debitis, quam nobis. Qui exercitationem, odio ipsam culpa laborum magnam incidunt eius perspiciatis natus! Repellendus veniam eos itaque omnis nobis consequatur sint perspiciatis soluta, cum eligendi maxime aut corrupti. Aspernatur dignissimos nesciunt iste optio in nulla, odit et fugiat magni qui quo veritatis consequuntur repellat deserunt labore libero saepe sit tenetur ipsam aperiam facilis ducimus quasi perferendis. Eligendi sunt sit dolor, repudiandae fugit officiis eaque reiciendis, vel nobis asperiores, sequi autem pariatur voluptate placeat nihil minima dolorum accusamus. Nisi quaerat libero possimus asperiores expedita dolorem voluptatem minima doloribus delectus aut iusto distinctio voluptatum ipsa in enim accusamus, similique sequi totam nemo atque deleniti illo maxime. Voluptas officia eveniet molestias, at pariatur odio. Sapiente rem praesentium quis harum mollitia assumenda eum rerum nemo illum fugit blanditiis aliquid, eveniet veritatis optio aut aspernatur atque nostrum omnis inventore. Magnam excepturi omnis debitis aliquam quasi inventore cupiditate consequuntur, magni repellendus, nostrum illo nemo illum amet in sit commodi doloribus facere temporibus, saepe at soluta quos nobis? Sapiente, nemo pariatur! Dicta animi, minima autem a perspiciatis maiores. Numquam voluptatem officia alias commodi pariatur unde odio eligendi et repudiandae tenetur aspernatur incidunt amet sequi ipsa, neque laborum? Numquam quae a hic nihil delectus inventore laboriosam ipsam molestiae, saepe sint veritatis sed et! Repudiandae mollitia eligendi quis reiciendis, id aperiam ipsum harum facilis error placeat. Modi harum necessitatibus odio veniam. Delectus odit eaque, laboriosam praesentium in fuga obcaecati consequatur adipisci excepturi numquam magnam! Possimus voluptate quisquam distinctio nulla qui, eaque quis obcaecati laboriosam vero, in fuga! Tempora molestias, aliquam nam ducimus, placeat mollitia quod ut quo reiciendis amet, architecto consectetur sequi sed adipisci doloribus enim nesciunt ratione consequuntur? Veritatis aliquid animi suscipit nostrum itaque doloremque ex eos libero quo veniam deleniti excepturi at, eligendi expedita corrupti recusandae labore accusantium? Sit, perferendis illum? Placeat repellat molestias aliquam est vitae dignissimos debitis modi similique maxime excepturi fuga consequuntur ratione, at ipsum eum id animi blanditiis eligendi dolor officia sit numquam iusto quibusdam mollitia? Optio necessitatibus exercitationem delectus veritatis, aspernatur velit. Nemo, ducimus? At eveniet magni ipsa labore quasi voluptatum architecto iusto eligendi nisi tempore, necessitatibus sed quidem alias eum, quaerat fugiat molestiae, quos libero quia. Quos, optio libero reprehenderit pariatur ipsam fugit aut harum debitis dolor animi, voluptate accusantium recusandae itaque voluptas praesentium illo. Non fuga reiciendis nobis doloribus soluta quos sed, temporibus doloremque illo magni, neque deleniti, est aut quidem labore asperiores in amet commodi ratione! Aspernatur exercitationem modi maiores similique rem! Dolore vero, nam excepturi saepe similique, sequi vel tenetur, magni quas beatae dicta molestiae dolor mollitia debitis accusamus accusantium ratione dignissimos nulla nostrum praesentium. Fuga nam quis ab libero nesciunt sunt odio, eveniet illum mollitia voluptas, vitae et, dicta reiciendis expedita eaque in nemo soluta possimus. Earum, asperiores nihil..</p>
                            <footer class="blockquote-footer "> Author Name</footer>
                            <a href="blog_details.php" class="btn btn-primary mb-1 text-decoration-none text-light">Read More
                                <i class="fa-solid fa-arrow-right ms-2" style="color: white"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 ">
                    <div class="card mb-4">
                        <a href="edit.php" class="edit"><i class="fa-solid fa-pen-to-square" style=" color: #7cbe45;"></i></a>
                        <a href="#" class="edit1"><i class="fa-solid fa-trash" style="color: red;"></i></i></a>

                        <a href="blog_details.php">
                            <img src="assets/images/image.jpeg" class="card-img-top">
                        </a>
                        <div class=" card-body">
                            <h5 class="card-title">This is Sample Title</h5>
                            <p class="card-text ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae odit repudiandae quis, voluptatem facere aut sint ipsam asperiores assumenda repellat voluptates modi eos quaerat, incidunt fugiat vel nihil, cupiditate illo rem qui laborum possimus quidem! Mollitia natus a magni voluptate libero deleniti facere dolorum ex officiis eum debitis, quam nobis. Qui exercitationem, odio ipsam culpa laborum magnam incidunt eius perspiciatis natus! Repellendus veniam eos itaque omnis nobis consequatur sint perspiciatis soluta, cum eligendi maxime aut corrupti. Aspernatur dignissimos nesciunt iste optio in nulla, odit et fugiat magni qui quo veritatis consequuntur repellat deserunt labore libero saepe sit tenetur ipsam aperiam facilis ducimus quasi perferendis. Eligendi sunt sit dolor, repudiandae fugit officiis eaque reiciendis, vel nobis asperiores, sequi autem pariatur voluptate placeat nihil minima dolorum accusamus. Nisi quaerat libero possimus asperiores expedita dolorem voluptatem minima doloribus delectus aut iusto distinctio voluptatum ipsa in enim accusamus, similique sequi totam nemo atque deleniti illo maxime. Voluptas officia eveniet molestias, at pariatur odio. Sapiente rem praesentium quis harum mollitia assumenda eum rerum nemo illum fugit blanditiis aliquid, eveniet veritatis optio aut aspernatur atque nostrum omnis inventore. Magnam excepturi omnis debitis aliquam quasi inventore cupiditate consequuntur, magni repellendus, nostrum illo nemo illum amet in sit commodi doloribus facere temporibus, saepe at soluta quos nobis? Sapiente, nemo pariatur! Dicta animi, minima autem a perspiciatis maiores. Numquam voluptatem officia alias commodi pariatur unde odio eligendi et repudiandae tenetur aspernatur incidunt amet sequi ipsa, neque laborum? Numquam quae a hic nihil delectus inventore laboriosam ipsam molestiae, saepe sint veritatis sed et! Repudiandae mollitia eligendi quis reiciendis, id aperiam ipsum harum facilis error placeat. Modi harum necessitatibus odio veniam. Delectus odit eaque, laboriosam praesentium in fuga obcaecati consequatur adipisci excepturi numquam magnam! Possimus voluptate quisquam distinctio nulla qui, eaque quis obcaecati laboriosam vero, in fuga! Tempora molestias, aliquam nam ducimus, placeat mollitia quod ut quo reiciendis amet, architecto consectetur sequi sed adipisci doloribus enim nesciunt ratione consequuntur? Veritatis aliquid animi suscipit nostrum itaque doloremque ex eos libero quo veniam deleniti excepturi at, eligendi expedita corrupti recusandae labore accusantium? Sit, perferendis illum? Placeat repellat molestias aliquam est vitae dignissimos debitis modi similique maxime excepturi fuga consequuntur ratione, at ipsum eum id animi blanditiis eligendi dolor officia sit numquam iusto quibusdam mollitia? Optio necessitatibus exercitationem delectus veritatis, aspernatur velit. Nemo, ducimus? At eveniet magni ipsa labore quasi voluptatum architecto iusto eligendi nisi tempore, necessitatibus sed quidem alias eum, quaerat fugiat molestiae, quos libero quia. Quos, optio libero reprehenderit pariatur ipsam fugit aut harum debitis dolor animi, voluptate accusantium recusandae itaque voluptas praesentium illo. Non fuga reiciendis nobis doloribus soluta quos sed, temporibus doloremque illo magni, neque deleniti, est aut quidem labore asperiores in amet commodi ratione! Aspernatur exercitationem modi maiores similique rem! Dolore vero, nam excepturi saepe similique, sequi vel tenetur, magni quas beatae dicta molestiae dolor mollitia debitis accusamus accusantium ratione dignissimos nulla nostrum praesentium. Fuga nam quis ab libero nesciunt sunt odio, eveniet illum mollitia voluptas, vitae et, dicta reiciendis expedita eaque in nemo soluta possimus. Earum, asperiores nihil..</p>
                            <footer class="blockquote-footer "> Author Name</footer>
                            <a href="blog_details.php" class="btn btn-primary mb-1 text-decoration-none text-light">Read More
                                <i class="fa-solid fa-arrow-right ms-2" style="color: white"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 ">
                    <div class="card mb-4">
                        <a href="edit.php" class="edit"><i class="fa-solid fa-pen-to-square" style=" color: #7cbe45;"></i></a>
                        <a href="#" class="edit1"><i class="fa-solid fa-trash" style="color: red;"></i></i></a>

                        <a href="blog_details.php">
                            <img src="assets/images/image.jpeg" class="card-img-top">
                        </a>
                        <div class=" card-body">
                            <h5 class="card-title">This is Sample Title</h5>
                            <p class="card-text ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae odit repudiandae quis, voluptatem facere aut sint ipsam asperiores assumenda repellat voluptates modi eos quaerat, incidunt fugiat vel nihil, cupiditate illo rem qui laborum possimus quidem! Mollitia natus a magni voluptate libero deleniti facere dolorum ex officiis eum debitis, quam nobis. Qui exercitationem, odio ipsam culpa laborum magnam incidunt eius perspiciatis natus! Repellendus veniam eos itaque omnis nobis consequatur sint perspiciatis soluta, cum eligendi maxime aut corrupti. Aspernatur dignissimos nesciunt iste optio in nulla, odit et fugiat magni qui quo veritatis consequuntur repellat deserunt labore libero saepe sit tenetur ipsam aperiam facilis ducimus quasi perferendis. Eligendi sunt sit dolor, repudiandae fugit officiis eaque reiciendis, vel nobis asperiores, sequi autem pariatur voluptate placeat nihil minima dolorum accusamus. Nisi quaerat libero possimus asperiores expedita dolorem voluptatem minima doloribus delectus aut iusto distinctio voluptatum ipsa in enim accusamus, similique sequi totam nemo atque deleniti illo maxime. Voluptas officia eveniet molestias, at pariatur odio. Sapiente rem praesentium quis harum mollitia assumenda eum rerum nemo illum fugit blanditiis aliquid, eveniet veritatis optio aut aspernatur atque nostrum omnis inventore. Magnam excepturi omnis debitis aliquam quasi inventore cupiditate consequuntur, magni repellendus, nostrum illo nemo illum amet in sit commodi doloribus facere temporibus, saepe at soluta quos nobis? Sapiente, nemo pariatur! Dicta animi, minima autem a perspiciatis maiores. Numquam voluptatem officia alias commodi pariatur unde odio eligendi et repudiandae tenetur aspernatur incidunt amet sequi ipsa, neque laborum? Numquam quae a hic nihil delectus inventore laboriosam ipsam molestiae, saepe sint veritatis sed et! Repudiandae mollitia eligendi quis reiciendis, id aperiam ipsum harum facilis error placeat. Modi harum necessitatibus odio veniam. Delectus odit eaque, laboriosam praesentium in fuga obcaecati consequatur adipisci excepturi numquam magnam! Possimus voluptate quisquam distinctio nulla qui, eaque quis obcaecati laboriosam vero, in fuga! Tempora molestias, aliquam nam ducimus, placeat mollitia quod ut quo reiciendis amet, architecto consectetur sequi sed adipisci doloribus enim nesciunt ratione consequuntur? Veritatis aliquid animi suscipit nostrum itaque doloremque ex eos libero quo veniam deleniti excepturi at, eligendi expedita corrupti recusandae labore accusantium? Sit, perferendis illum? Placeat repellat molestias aliquam est vitae dignissimos debitis modi similique maxime excepturi fuga consequuntur ratione, at ipsum eum id animi blanditiis eligendi dolor officia sit numquam iusto quibusdam mollitia? Optio necessitatibus exercitationem delectus veritatis, aspernatur velit. Nemo, ducimus? At eveniet magni ipsa labore quasi voluptatum architecto iusto eligendi nisi tempore, necessitatibus sed quidem alias eum, quaerat fugiat molestiae, quos libero quia. Quos, optio libero reprehenderit pariatur ipsam fugit aut harum debitis dolor animi, voluptate accusantium recusandae itaque voluptas praesentium illo. Non fuga reiciendis nobis doloribus soluta quos sed, temporibus doloremque illo magni, neque deleniti, est aut quidem labore asperiores in amet commodi ratione! Aspernatur exercitationem modi maiores similique rem! Dolore vero, nam excepturi saepe similique, sequi vel tenetur, magni quas beatae dicta molestiae dolor mollitia debitis accusamus accusantium ratione dignissimos nulla nostrum praesentium. Fuga nam quis ab libero nesciunt sunt odio, eveniet illum mollitia voluptas, vitae et, dicta reiciendis expedita eaque in nemo soluta possimus. Earum, asperiores nihil..</p>
                            <footer class="blockquote-footer "> Author Name</footer>
                            <a href="blog_details.php" class="btn btn-primary mb-1 text-decoration-none text-light">Read More
                                <i class="fa-solid fa-arrow-right ms-2" style="color: white"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 ">
                    <div class="card mb-4">
                        <a href="edit.php" class="edit"><i class="fa-solid fa-pen-to-square" style=" color: #7cbe45;"></i></a>
                        <a href="#" class="edit1"><i class="fa-solid fa-trash" style="color: red;"></i></i></a>

                        <a href="blog_details.php">
                            <img src="assets/images/image.jpeg" class="card-img-top">
                        </a>
                        <div class=" card-body">
                            <h5 class="card-title">This is Sample Title</h5>
                            <p class="card-text ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae odit repudiandae quis, voluptatem facere aut sint ipsam asperiores assumenda repellat voluptates modi eos quaerat, incidunt fugiat vel nihil, cupiditate illo rem qui laborum possimus quidem! Mollitia natus a magni voluptate libero deleniti facere dolorum ex officiis eum debitis, quam nobis. Qui exercitationem, odio ipsam culpa laborum magnam incidunt eius perspiciatis natus! Repellendus veniam eos itaque omnis nobis consequatur sint perspiciatis soluta, cum eligendi maxime aut corrupti. Aspernatur dignissimos nesciunt iste optio in nulla, odit et fugiat magni qui quo veritatis consequuntur repellat deserunt labore libero saepe sit tenetur ipsam aperiam facilis ducimus quasi perferendis. Eligendi sunt sit dolor, repudiandae fugit officiis eaque reiciendis, vel nobis asperiores, sequi autem pariatur voluptate placeat nihil minima dolorum accusamus. Nisi quaerat libero possimus asperiores expedita dolorem voluptatem minima doloribus delectus aut iusto distinctio voluptatum ipsa in enim accusamus, similique sequi totam nemo atque deleniti illo maxime. Voluptas officia eveniet molestias, at pariatur odio. Sapiente rem praesentium quis harum mollitia assumenda eum rerum nemo illum fugit blanditiis aliquid, eveniet veritatis optio aut aspernatur atque nostrum omnis inventore. Magnam excepturi omnis debitis aliquam quasi inventore cupiditate consequuntur, magni repellendus, nostrum illo nemo illum amet in sit commodi doloribus facere temporibus, saepe at soluta quos nobis? Sapiente, nemo pariatur! Dicta animi, minima autem a perspiciatis maiores. Numquam voluptatem officia alias commodi pariatur unde odio eligendi et repudiandae tenetur aspernatur incidunt amet sequi ipsa, neque laborum? Numquam quae a hic nihil delectus inventore laboriosam ipsam molestiae, saepe sint veritatis sed et! Repudiandae mollitia eligendi quis reiciendis, id aperiam ipsum harum facilis error placeat. Modi harum necessitatibus odio veniam. Delectus odit eaque, laboriosam praesentium in fuga obcaecati consequatur adipisci excepturi numquam magnam! Possimus voluptate quisquam distinctio nulla qui, eaque quis obcaecati laboriosam vero, in fuga! Tempora molestias, aliquam nam ducimus, placeat mollitia quod ut quo reiciendis amet, architecto consectetur sequi sed adipisci doloribus enim nesciunt ratione consequuntur? Veritatis aliquid animi suscipit nostrum itaque doloremque ex eos libero quo veniam deleniti excepturi at, eligendi expedita corrupti recusandae labore accusantium? Sit, perferendis illum? Placeat repellat molestias aliquam est vitae dignissimos debitis modi similique maxime excepturi fuga consequuntur ratione, at ipsum eum id animi blanditiis eligendi dolor officia sit numquam iusto quibusdam mollitia? Optio necessitatibus exercitationem delectus veritatis, aspernatur velit. Nemo, ducimus? At eveniet magni ipsa labore quasi voluptatum architecto iusto eligendi nisi tempore, necessitatibus sed quidem alias eum, quaerat fugiat molestiae, quos libero quia. Quos, optio libero reprehenderit pariatur ipsam fugit aut harum debitis dolor animi, voluptate accusantium recusandae itaque voluptas praesentium illo. Non fuga reiciendis nobis doloribus soluta quos sed, temporibus doloremque illo magni, neque deleniti, est aut quidem labore asperiores in amet commodi ratione! Aspernatur exercitationem modi maiores similique rem! Dolore vero, nam excepturi saepe similique, sequi vel tenetur, magni quas beatae dicta molestiae dolor mollitia debitis accusamus accusantium ratione dignissimos nulla nostrum praesentium. Fuga nam quis ab libero nesciunt sunt odio, eveniet illum mollitia voluptas, vitae et, dicta reiciendis expedita eaque in nemo soluta possimus. Earum, asperiores nihil..</p>
                            <footer class="blockquote-footer "> Author Name</footer>
                            <a href="blog_details.php" class="btn btn-primary mb-1 text-decoration-none text-light">Read More
                                <i class="fa-solid fa-arrow-right ms-2" style="color: white"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 ">
                    <div class="card mb-4">
                        <a href="edit.php" class="edit"><i class="fa-solid fa-pen-to-square" style=" color: #7cbe45;"></i></a>
                        <a href="#" class="edit1"><i class="fa-solid fa-trash" style="color: red;"></i></i></a>

                        <a href="blog_details.php">
                            <img src="assets/images/image.jpeg" class="card-img-top">
                        </a>
                        <div class=" card-body">
                            <h5 class="card-title">This is Sample Title</h5>
                            <p class="card-text ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae odit repudiandae quis, voluptatem facere aut sint ipsam asperiores assumenda repellat voluptates modi eos quaerat, incidunt fugiat vel nihil, cupiditate illo rem qui laborum possimus quidem! Mollitia natus a magni voluptate libero deleniti facere dolorum ex officiis eum debitis, quam nobis. Qui exercitationem, odio ipsam culpa laborum magnam incidunt eius perspiciatis natus! Repellendus veniam eos itaque omnis nobis consequatur sint perspiciatis soluta, cum eligendi maxime aut corrupti. Aspernatur dignissimos nesciunt iste optio in nulla, odit et fugiat magni qui quo veritatis consequuntur repellat deserunt labore libero saepe sit tenetur ipsam aperiam facilis ducimus quasi perferendis. Eligendi sunt sit dolor, repudiandae fugit officiis eaque reiciendis, vel nobis asperiores, sequi autem pariatur voluptate placeat nihil minima dolorum accusamus. Nisi quaerat libero possimus asperiores expedita dolorem voluptatem minima doloribus delectus aut iusto distinctio voluptatum ipsa in enim accusamus, similique sequi totam nemo atque deleniti illo maxime. Voluptas officia eveniet molestias, at pariatur odio. Sapiente rem praesentium quis harum mollitia assumenda eum rerum nemo illum fugit blanditiis aliquid, eveniet veritatis optio aut aspernatur atque nostrum omnis inventore. Magnam excepturi omnis debitis aliquam quasi inventore cupiditate consequuntur, magni repellendus, nostrum illo nemo illum amet in sit commodi doloribus facere temporibus, saepe at soluta quos nobis? Sapiente, nemo pariatur! Dicta animi, minima autem a perspiciatis maiores. Numquam voluptatem officia alias commodi pariatur unde odio eligendi et repudiandae tenetur aspernatur incidunt amet sequi ipsa, neque laborum? Numquam quae a hic nihil delectus inventore laboriosam ipsam molestiae, saepe sint veritatis sed et! Repudiandae mollitia eligendi quis reiciendis, id aperiam ipsum harum facilis error placeat. Modi harum necessitatibus odio veniam. Delectus odit eaque, laboriosam praesentium in fuga obcaecati consequatur adipisci excepturi numquam magnam! Possimus voluptate quisquam distinctio nulla qui, eaque quis obcaecati laboriosam vero, in fuga! Tempora molestias, aliquam nam ducimus, placeat mollitia quod ut quo reiciendis amet, architecto consectetur sequi sed adipisci doloribus enim nesciunt ratione consequuntur? Veritatis aliquid animi suscipit nostrum itaque doloremque ex eos libero quo veniam deleniti excepturi at, eligendi expedita corrupti recusandae labore accusantium? Sit, perferendis illum? Placeat repellat molestias aliquam est vitae dignissimos debitis modi similique maxime excepturi fuga consequuntur ratione, at ipsum eum id animi blanditiis eligendi dolor officia sit numquam iusto quibusdam mollitia? Optio necessitatibus exercitationem delectus veritatis, aspernatur velit. Nemo, ducimus? At eveniet magni ipsa labore quasi voluptatum architecto iusto eligendi nisi tempore, necessitatibus sed quidem alias eum, quaerat fugiat molestiae, quos libero quia. Quos, optio libero reprehenderit pariatur ipsam fugit aut harum debitis dolor animi, voluptate accusantium recusandae itaque voluptas praesentium illo. Non fuga reiciendis nobis doloribus soluta quos sed, temporibus doloremque illo magni, neque deleniti, est aut quidem labore asperiores in amet commodi ratione! Aspernatur exercitationem modi maiores similique rem! Dolore vero, nam excepturi saepe similique, sequi vel tenetur, magni quas beatae dicta molestiae dolor mollitia debitis accusamus accusantium ratione dignissimos nulla nostrum praesentium. Fuga nam quis ab libero nesciunt sunt odio, eveniet illum mollitia voluptas, vitae et, dicta reiciendis expedita eaque in nemo soluta possimus. Earum, asperiores nihil..</p>
                            <footer class="blockquote-footer "> Author Name</footer>
                            <a href="blog_details.php" class="btn btn-primary mb-1 text-decoration-none text-light">Read More
                                <i class="fa-solid fa-arrow-right ms-2" style="color: white"></i>
                            </a>
                        </div>
                    </div>
                </div> -->

                



            </div>
            <nav aria-label="Page navigation example ">
                <ul class="pagination justify-content-end mt-5">
                    <li class="page-item ">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item  active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item  "><a class="page-link" href="#">3</a></li>
                    <li class="page-item  ">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
    </section>
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>